Репозиторий для работы c курсом [Netology DevOps-инженер](https://netology.ru/programs/devops).

Полезные заметки:
* [По командам git](/src/gitnotes.md)

## Homeworks
* [1.1 Введение в DevOps](/src/homework/1.1)
* [2.1 Системы контроля версий](/src/homeworks/2.1)